﻿using UnityEngine;
using UnityEngine.UI;

public class LoadImageRemote : MonoBehaviour
{
    MeshRenderer m_Renderer;
    Texture2D myTexture;
    Shader shaderDiffuse; 
    // Use this for initialization
    void Start()
    {
        m_Renderer = GetComponent<MeshRenderer>();
        shaderDiffuse = Shader.Find("UI/Default");
        m_Renderer.material.shader = shaderDiffuse;
        myTexture = Resources.Load("Images/ecremote") as Texture2D;        
        m_Renderer.material.mainTexture = myTexture;
        Debug.Log("text x " + m_Renderer.material.mainTexture.width + " y " + m_Renderer.material.mainTexture.height);
        Debug.Log("dim" + m_Renderer.material.mainTexture.dimension.ToString());
    }
}