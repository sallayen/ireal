﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InriaRemote : MonoBehaviour {
    public GameObject temp;
    public GameObject occupied;
    public GameObject ventilation;
    public GameObject InriaRemoteGlobal;
    public bool tempOn;
    public bool occupiedOn;
    public bool ventilationOn;
    public bool ECRemoteMenuOn;
    // Use this for initialization
    void Start () {
        /*temp = GameObject.Find("Temp");
        occupied = GameObject.Find("Occupied");
        ventilation = GameObject.Find("Ventilation");*/
        tempOn = false;
        occupiedOn = false;
        ventilationOn = false;
        ECRemoteMenuOn = false;
    }

    public void VentilationTapped()
    {
        Debug.Log("vent tapped");
        occupiedOn = false;
        tempOn = false;
        temp.SetActive(ventilationOn);
        occupied.SetActive(tempOn);

        ventilationOn = !ventilationOn;
        ventilation.SetActive(ventilationOn);
    }

    public void TempTapped()
    {
        Debug.Log("temp tapped");
        occupiedOn = false;
        ventilationOn = false;
        ventilation.SetActive(ventilationOn);
        occupied.SetActive(tempOn);

        tempOn = !tempOn;
        temp.SetActive(tempOn);
    }

    public void OccupiedTapped()
    {
        Debug.Log("occupied tapped");
        tempOn = false;
        ventilationOn = false;
        ventilation.SetActive(ventilationOn);
        temp.SetActive(tempOn);

        occupiedOn = !occupiedOn;
        occupied.SetActive(occupiedOn);
    }

    public void ExitTapped()
    {
        Debug.Log("exit tapped");
        tempOn = false;
        occupiedOn = false;
        ventilationOn = false;
        ECRemoteMenuOn = false;
        ventilation.SetActive(ventilationOn);
        temp.SetActive(tempOn);
        occupied.SetActive(occupiedOn);
        InriaRemoteGlobal.SetActive(ECRemoteMenuOn);
    }

    // Update is called once per frame
    void Update () {
        
    }
}
