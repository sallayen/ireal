﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorBlink : MonoBehaviour {
    MeshRenderer m_Renderer;
    Shader shaderDiffuse;
    public float speed;
    // Use this for initialization
    void Start () {
        m_Renderer = GetComponent<MeshRenderer>();
        //shaderDiffuse = Shader.Find("Transparent/Diffuse");
        shaderDiffuse = Shader.Find("UI/Default");
        m_Renderer.material.shader = shaderDiffuse;
        speed = 4.0f;        
    }
	
	// Update is called once per frame
	void Update () {        
        m_Renderer.material.color = Color.Lerp(Color.clear, Color.red, Mathf.Sin(Time.time * speed));
        //Debug.Log("blinking");
    }
}
