﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InriaRemoteSelected : MonoBehaviour
{
    GameObject inriaRemote;
    bool inriaRemoteOn;
    public void Start()
    {
        InitInriaRemote();
    }

    public void InriaRemoteTapped()
    {
        Debug.Log("5");
        inriaRemoteOn = !inriaRemoteOn;
        Debug.Log("6");
        inriaRemote.SetActive(inriaRemoteOn);
        Debug.Log("Inria remote tapped!");
    }

    public void InitInriaRemote()
    {
        if (GameObject.Find("InriaRemoteObject") != null)
        {
            inriaRemote = GameObject.Find("InriaRemoteObject");
            inriaRemoteOn = false;
            inriaRemote.SetActive(inriaRemoteOn);
        }
        else
        {
            Debug.Log("Can not find InriaRemoteObject");
        }
    }

    public void UpdatePosMenu(Vector3 position)
    {
        InitInriaRemote();
        Debug.Log("1");
        Vector3 pos = position;
        Debug.Log("2");
        pos.y = pos.y + 0.1f;
        Debug.Log("3");
        if (inriaRemote != null)
        {
            inriaRemote.transform.position = pos;
        }
        else
        {
            Debug.Log("Inria remote is null");
        }            
        Debug.Log("4");
    }

    public void Update()
    {

    }
}
