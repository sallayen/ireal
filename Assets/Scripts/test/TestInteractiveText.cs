﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using HoloToolkit.Unity.InputModule;
using UnityEngine.XR.WSA.Input;

namespace HoloToolkit.Examples.InteractiveElements
{
    public class TestInteractiveText : MonoBehaviour//, IInputClickHandler, IInputHandler
    {
        Font myFont;
        string nameLabel;
        GameObject myTextObject;
        TextMesh textMeshComponent;
        MeshRenderer meshRendererComponent;
        BoxCollider boxCollider;
        Interactive interactive;
        InriaRemoteSelected inriaRemoteSelected;
        GestureRecognizer recognizer;
        bool isOver;


        // Use this for initialization
        void Awake()
        {
            isOver = false;
            // Set up a GestureRecognizer to detect Select gestures.
            recognizer = new GestureRecognizer();
            recognizer.Tapped += (args) =>
            {
                if (isOver)
                {
                    Tapped();
                }
            };
            recognizer.StartCapturingGestures();
        }

        // Use this for initialization
        void Start()
        {
            inriaRemoteSelected = new InriaRemoteSelected();
            //inriaRemoteSelected.Start();
            //interactive = new Interactive();
            nameLabel = "Test3DText";
            // Create new object to display 3d text
            myTextObject = new GameObject(nameLabel);
            myTextObject.AddComponent<TextMesh>();
            /*myTextObject.AddComponent<Interactive>();
            myTextObject.AddComponent<InriaRemoteSelected>();*/
            // Get components
            /*interactive = myTextObject.GetComponent(typeof(Interactive)) as Interactive;
            inriaRemoteSelected = myTextObject.GetComponent(typeof(InriaRemoteSelected)) as InriaRemoteSelected;
            if (interactive.OnSelectEvents == null)
                interactive.OnSelectEvents = new UnityEvent();
            interactive.OnSelectEvents.AddListener(inriaRemoteSelected.InriaRemoteTapped);*/

            textMeshComponent = myTextObject.GetComponent(typeof(TextMesh)) as TextMesh;
            meshRendererComponent = myTextObject.GetComponent(typeof(MeshRenderer)) as MeshRenderer;
            // Set font of TextMesh component (it works according to the inspector)
            myFont = (Font)Resources.Load("Fonts/Modern Vision");
            textMeshComponent.font = myFont;
            textMeshComponent.color = UnityEngine.Color.red;
            textMeshComponent.transform.localScale = new Vector3(0.007f, 0.007f, 0.007f);
            textMeshComponent.fontSize = 100;
            textMeshComponent.anchor = TextAnchor.MiddleCenter;
            textMeshComponent.alignment = TextAlignment.Center;
            myTextObject.GetComponent<Renderer>().material = myFont.material;

            textMeshComponent.text = "Test3DText";

            boxCollider = myTextObject.AddComponent<BoxCollider>();
            boxCollider.isTrigger = true;
            textMeshComponent.transform.position = new Vector3(0.0f, 0.0f, 3.0f);
        }

        void Tapped()
        {
            Debug.Log("tap");
            inriaRemoteSelected.UpdatePosMenu(textMeshComponent.transform.position);
            inriaRemoteSelected.InriaRemoteTapped();
        }

        // Update is called once per frame
        void Update()
        {
            // Do a raycast into the world based on the user's
            // head position and orientation.
            var headPosition = Camera.main.transform.position;
            var gazeDirection = Camera.main.transform.forward;

            RaycastHit hitInfo;
            if (Physics.Raycast(headPosition, gazeDirection, out hitInfo))
            {
                if (hitInfo.collider.gameObject.name.Equals(nameLabel))
                {
                    textMeshComponent.color = UnityEngine.Color.white;
                    isOver = true;
                    //interactive.OnSelectEvents.Invoke();
                }
                else
                {
                    isOver = false;
                    textMeshComponent.color = UnityEngine.Color.red;
                }
            }
        }
    }
    
}
