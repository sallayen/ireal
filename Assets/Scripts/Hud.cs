﻿using CognitiveServices;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.WSA.WebCam;
using System.Threading.Tasks;
using System.Threading;
//using UnityEngine.XR.WSA.Input;

#if !UNITY_EDITOR && UNITY_WINRT_10_0

using System.Net.Http;
using System.Net.Http.Headers;

#endif

public class Hud : MonoBehaviour
{
    public Font myFont;
    public Text InfoPanel;
    public Text DebugPanel;
    public Resolution m_cameraResolution;
    public PhotoCapture photoCaptureObject = null;
    List<Label> facesLabel;
    List<Label> objLabel;    
    Matrix4x4 cameraToWorldMatrix;
    Matrix4x4 projectionMatrix;
    public bool displayBB;
    public bool staticObjectMode;
    public List<byte> imageBufferList;
    //public Texture2D sharedTexture;
    public bool captureOnOff;
    public bool showLines;
    public List<Line> lines;
    int countObject;
    public GameObject InriaRemoteMenu;
#if !UNITY_EDITOR && UNITY_WINRT_10_0

    HttpClient client = new HttpClient();

#endif

    //public string _subscriptionKey = "19a98d2a04b84635be57de4c3ba7126d";
    //string _computerVisionEndpoint = "titanic.paris.inria.fr:9099";
    // API Adresse for Face Detection in gpu
    //string _computerVisionEndpoint = "titanic.paris.inria.fr:9099/api/cv/face_detection_cnn/";
    // API Adresse for Face Recognition
    // string _computerVisionEndpoint = "http://titanic.paris.inria.fr:9099/api/cv/face_recognition/";
    public string _computerVisionEndpoint = "http://titanic.paris.inria.fr:9099/api/cv/face_object_recognition/";
    string historyAddress;
    //GestureRecognizer recognizer;

    // Use this for initialization
    /*void Awake()
    {

        // Set up a GestureRecognizer to detect Select gestures.
        recognizer = new GestureRecognizer();
        recognizer.Tapped += (args) =>
        {
            Debug.Log("tap");
        };
        recognizer.StartCapturingGestures();
    }*/

    void Start()
    {
        DebugPanel.text = "Connecting to titanic.paris.inria.fr";
        facesLabel = new List<Label>();
        objLabel = new List<Label>();
        imageBufferList = new List<byte>();
        lines = new List<Line>();
        historyAddress = _computerVisionEndpoint;
        //sharedTexture = new Texture2D(896,504);
        displayBB = false;
        staticObjectMode = false;
        captureOnOff = true;
        showLines = false;
        countObject = 0;
        Debug.Log("Start");        
        /*Vector3 test1 = new Vector3(-0.7f, 0.4f, 3f);
        Vector3 test2 = new Vector3(0.5f, 0.5f, 3f);
        lines.Add(new Line(test1, test2));
        test1 = new Vector3(-0.7f, -0.4f, 3f);
        test2 = new Vector3(0.5f, -0.5f, 3f);
        lines.Add(new Line(test1, test2));*/
        if (captureOnOff)
        {
            PhotoCapture.CreateAsync(false, OnPhotoCaptureCreated);
        }        
    }

    void OnPhotoCaptureCreated(PhotoCapture captureObject)
    {
        photoCaptureObject = captureObject;

        m_cameraResolution = PhotoCapture.SupportedResolutions.OrderByDescending((res) => res.width * res.height).Last();

        CameraParameters c = new CameraParameters();
        c.hologramOpacity = 0.0f;
        c.cameraResolutionWidth = m_cameraResolution.width;
        c.cameraResolutionHeight = m_cameraResolution.height;
        c.pixelFormat = CapturePixelFormat.JPEG;
        Debug.Log("OnPhotoCaptureCreated");
        captureObject.StartPhotoModeAsync(c, OnPhotoModeStarted);
    }

    void OnStoppedPhotoMode(PhotoCapture.PhotoCaptureResult result)
    {
        photoCaptureObject.Dispose();
        photoCaptureObject = null;
    }

    private void OnPhotoModeStarted(PhotoCapture.PhotoCaptureResult result)
    {
        if (result.success)
        {
            Debug.Log("OnPhotoModeStarted sucess");
            if (captureOnOff)
            {
                photoCaptureObject.TakePhotoAsync(OnCapturedPhotoToMemory);
            }            
        }
        else
        {
            Debug.LogError("Unable to start photo mode!");
        }
    }

    public void StartPhotoMode()
    {
        PhotoCapture.CreateAsync(false, OnPhotoCaptureCreated);
    }

    public void StopPhotoMode()
    {
        photoCaptureObject.StopPhotoModeAsync(OnStoppedPhotoMode);
    }


    async void OnCapturedPhotoToMemory(PhotoCapture.PhotoCaptureResult result, PhotoCaptureFrame photoCaptureFrame)
    {
        if (result.success)
        {
            //List<byte> imageBufferList = new List<byte>();
            // Copy the raw IMFMediaBuffer data into our empty byte list.
            photoCaptureFrame.CopyRawImageDataIntoBuffer(imageBufferList);
            //photoCaptureFrame.UploadImageDataToTexture(sharedTexture);
            //Debug.Log("photo taken");
            photoCaptureFrame.TryGetCameraToWorldMatrix(out cameraToWorldMatrix);
            photoCaptureFrame.TryGetProjectionMatrix(out projectionMatrix);
#if !UNITY_EDITOR && UNITY_WINRT_10_0
            checkAddress(); //we verify if the address is changed and reset face or object in function
            HttpResponseMessage response = await UploadImage(_computerVisionEndpoint, imageBufferList.ToArray());
            response.EnsureSuccessStatusCode();

            string responseBody = await response.Content.ReadAsStringAsync();
            AnalyzeJSON(responseBody);

            if (captureOnOff)
            {
                photoCaptureObject.TakePhotoAsync(OnCapturedPhotoToMemory);
            }
#endif
        }
        // Clean up
        //photoCaptureObject.StopPhotoModeAsync(OnStoppedPhotoMode);    
    }

    void AnalyzeJSON(string response)
    {
        var myObject = JsonUtility.FromJson<AnalysisResult>(response);
        List<string> faces = new List<string>();
        if (myObject != null)
        {
            objectTreatment(myObject.objects);
            faceTreatment(myObject.faces);
        }
        else
        {
            Debug.Log("OBJECT NULL");
        }
    }

    void objectTreatment(CognitiveServices.Object[] objects)
    {
        /*
        if (objects.Length > 0)
        {
            // remove all object labels
            ResetObjParameters();

            foreach (var obj in objects)
            {
                objLabel.Add(new Label(obj, m_cameraResolution, displayBB));
                //Debug.Log("name " + obj.name + " score " + obj.score + " box " + obj.box);
            }
        }
        */
        /*
        if (objects.Length > 0)
        {
            //bool objInList = false;
            foreach (var obj in objects)
            {
                bool objInList = false;
                foreach (var oLabel in objLabel.ToList())
                {
                    int index = objLabel.ToList().IndexOf(oLabel);
                    if (oLabel.nameLabel.Equals(obj.name))
                    {                        
                        objInList = true;
                        //update pos and rot 
                        oLabel.UpdatePosPx(obj);
                    }
                }
                if (!objInList)
                {
                    //add face pos and rot 
                    objLabel.Add(new Label(obj, m_cameraResolution, displayBB));
                }
            }
        }
        else
        {
            //reset position label and average                
            ResetFaceParameters();
        }
        //remove old face label if not present inside the json
        foreach (var oLabel in objLabel.ToList())
        {
            bool isPresent = false;
            foreach (var obj in objects.ToList())
            {
                if (oLabel.nameLabel.Equals(obj.name))
                {
                    isPresent = true;
                }
            }
            if (!isPresent)
            {
                //remove old label
                oLabel.Destroy();
                objLabel.Remove(oLabel);
            }
        }
        */
        if (staticObjectMode)
        {
            if (objects.Length > 0)
            {
                //bool objInList = false;
                foreach (var obj in objects)
                {
                    if (showLines)
                    {
                        lines.Add(new Line(obj, m_cameraResolution, projectionMatrix, cameraToWorldMatrix));
                    }
                    bool objInList = false;
                    foreach (var oLabel in objLabel.ToList())
                    {
                        //if (oLabel.isSameNamePos(obj, projectionMatrix, cameraToWorldMatrix))
                        //if (oLabel.nameLabel.Equals(obj.name) && oLabel.isSamePos(obj, projectionMatrix, cameraToWorldMatrix))
                        if (oLabel.nameLabel.Equals(obj.name) && oLabel.isSamePos(obj, projectionMatrix, cameraToWorldMatrix))
                        {
                            objInList = true;
                            //update pos and rot 
                            if (!oLabel.isStaticObject)
                            {
                                oLabel.UpdatePosPx(obj);
                            }
                            //Debug.Log("object updated");
                        }
                    }
                    if (!objInList)
                    {
                        //add face pos and rot 
                        countObject++;
                        objLabel.Add(new Label(obj, m_cameraResolution, displayBB, projectionMatrix, cameraToWorldMatrix, countObject));
                        //Debug.Log("object added");
                    }
                }
            }
            else
            {
                //reset position label and average                
                ResetObjParameters();
            }
            //remove old face label if not present inside the json
            foreach (var oLabel in objLabel.ToList())
            {
                bool isPresent = false;
                foreach (var obj in objects.ToList())
                {
                    //if (oLabel.isSameNamePos(obj, projectionMatrix, cameraToWorldMatrix))
                    //if (oLabel.nameLabel.Equals(obj.name))
                    if (oLabel.nameLabel.Equals(obj.name) && oLabel.isSamePos(obj, projectionMatrix, cameraToWorldMatrix))
                    {
                        isPresent = true;
                    }
                }
                if (!isPresent)
                {
                    if (!oLabel.isStaticObject)
                    {
                        //remove old label
                        oLabel.Destroy();
                        objLabel.Remove(oLabel);
                    }
                }
            }
        }
        // if not static mode object we erase the objects each frame
        else
        {
            if (objects.Length > 0)
            {
                // remove all object labels
                ResetObjParameters();

                foreach (var obj in objects)
                {
                    if (showLines)
                    {
                        lines.Add(new Line(obj, m_cameraResolution, projectionMatrix, cameraToWorldMatrix));
                    }
                    countObject++;
                    objLabel.Add(new Label(obj, m_cameraResolution, displayBB, projectionMatrix, cameraToWorldMatrix, countObject));
                }
            }
        }
    }

    void faceTreatment(CognitiveServices.Face[] faces)
    {
        if (faces.Length > 0)
        {
            foreach (var face in faces)
            {
                bool faceInList = false;
                foreach (var fLabel in facesLabel.ToList())
                {
                    if (fLabel.nameLabel.Equals(face.faceId))
                    {
                        faceInList = true;
                        //update pos and rot 
                        fLabel.UpdatePosPx(face);
                    }
                }
                if (!faceInList)
                {
                    //add face pos and rot 
                    facesLabel.Add(new Label(face, m_cameraResolution, displayBB, projectionMatrix, cameraToWorldMatrix));
                }
            }
        }
        else
        {
            //reset position label and average                
            ResetFaceParameters();
        }
        //remove old face label if not present inside the json
        foreach (var fLabel in facesLabel.ToList())
        {
            bool isPresent = false;
            foreach (var face in faces.ToList())
            {
                if (fLabel.nameLabel.Equals(face.faceId))
                {
                    isPresent = true;
                }
            }
            if (!isPresent)
            {
                //remove old label
                fLabel.Destroy();
                facesLabel.Remove(fLabel);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        DisplayAllLabel();
    }

#if !UNITY_EDITOR && UNITY_WINRT_10_0
    async public Task<HttpResponseMessage> UploadImage(string url, byte[] ImageData)
    {
        var requestContent = new MultipartFormDataContent();
        //    here you can specify boundary if you need---^
        var imageContent = new ByteArrayContent(ImageData);
        imageContent.Headers.ContentType = MediaTypeHeaderValue.Parse("image/jpeg");

        requestContent.Add(imageContent, "image", "image.jpg");

        return await client.PostAsync(url, requestContent);
    }
#endif

    void DisplayAllLabel()
    {
        //objects
        foreach (var oLabel in objLabel)
        {
            oLabel.SetPosRotLabel(projectionMatrix, cameraToWorldMatrix);
        }
        //faces
        foreach (var fLabel in facesLabel)
        {
            fLabel.SetPosRotLabel(projectionMatrix, cameraToWorldMatrix);
        }
    }

    public void ResetFaceParameters()
    {
        // remove all labels
        foreach (var fLabel in facesLabel.ToList())
        {
            fLabel.Destroy();
            facesLabel.Remove(fLabel);
        }
        facesLabel.Clear();
    }

    public void ResetObjParameters()
    {
        if (staticObjectMode)
        {
            // remove all labels except static object
            foreach (var oLabel in objLabel.ToList())
            {
                if (!oLabel.isStaticObject)
                {
                    oLabel.Destroy();
                    objLabel.Remove(oLabel);
                }
            }
        }
        else
        {
            // remove all labels
            foreach (var oLabel in objLabel.ToList())
            {
                oLabel.Destroy();
                objLabel.Remove(oLabel);
            }
            objLabel.Clear();
        }
    }

    public void ResetAllParameters()
    {
        ResetFaceParameters();
        ResetObjParameters();
    }

    void checkAddress()
    {
        bool isChanged;
        if (historyAddress == _computerVisionEndpoint)
        {
            isChanged = false;
        }
        else
        {
            isChanged = true;
            historyAddress = _computerVisionEndpoint;
        }
        if (isChanged)
        {
            if (_computerVisionEndpoint == "http://titanic.paris.inria.fr:9099/api/cv/face_recognition/")
            {
                ResetObjParameters();
            }
            if (_computerVisionEndpoint == "http://titanic.paris.inria.fr:9099/api/cv/object_recognition/")
            {
                ResetFaceParameters();
            }
        }
    }

    public void BoundingBoxIsOn()
    {
        displayBB = true;
        //objects
        foreach (var oLabel in objLabel)
        {
            oLabel.boundingBox.SetActive(displayBB);
        }
        //faces
        foreach (var fLabel in facesLabel)
        {
            fLabel.boundingBox.SetActive(displayBB);
        }
    }

    public void BoundingBoxIsOff()
    {
        displayBB = false;
        //objects
        foreach (var oLabel in objLabel)
        {
            oLabel.boundingBox.SetActive(displayBB);
        }
        //faces
        foreach (var fLabel in facesLabel)
        {
            fLabel.boundingBox.SetActive(displayBB);
        }
    }

    public void ClearLines()
    {
        // remove all lines
        foreach (var line in lines.ToList())
        {
            line.Destroy();
            lines.Remove(line);
        }
        lines.Clear();
    }
}
