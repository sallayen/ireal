﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CognitiveServices;
using UnityEngine.XR.WSA.Input;

public class Label : MonoBehaviour {

    public Vector3 pos;
    public Vector3 histPos;
    public Vector3 startPos;
    public Vector3 endPos;
    public float lerpTimeMove = 0.2f;
    public float currentLerpTimeMove;
    public float distance;
    public int countAverage;
    public int countAverageDistance;
    public int maxAverageSize = 60;
    public List<Vector3> bufferAverage;
    public List<float> bufferAverageDistance;
    public Resolution m_cameraResolution;
    public Font myFont;
    public string nameLabel;
    public GameObject myTextObject;
    public TextMesh textMeshComponent;
    public MeshRenderer meshRendererComponent;
    public bool isObj;
    public bool isFace;
    public GameObject boundingBox;
    public Vector3 posBB;
    public Vector3 startPosBB;
    public Vector3 endPosBB;
    public float lerpTimeMoveBB = 0.2f;
    public float currentLerpTimeMoveBB;
    public int width, height, resX, resY;
    public bool isStaticObject;
    int countStaticObject;
    BoxCollider boxCollider;

    GestureRecognizer recognizer;
    bool isOver;


    // Use this for initialization
    void Awake()
    {
        isOver = false;
        // Set up a GestureRecognizer to detect Select gestures.
        recognizer = new GestureRecognizer();
        recognizer.Tapped += (args) =>
        {
            if (isOver)
            {
                Tapped();
            }
        };
        recognizer.StartCapturingGestures();
    }
    public Label(Face face, Resolution resolution, bool displayBoundingBox, Matrix4x4 projectionMatrix, Matrix4x4 cameraToWorldMatrix)
    {
        isStaticObject = false;
        countStaticObject = 0;
        isFace = true;
        isObj = false;
        nameLabel = face.faceId;
        m_cameraResolution = resolution;
        // Create new object to display 3d text
        myTextObject = new GameObject(nameLabel);
        myTextObject.AddComponent<TextMesh>();
        
        
        // Get components
        textMeshComponent = myTextObject.GetComponent(typeof(TextMesh)) as TextMesh;
        meshRendererComponent = myTextObject.GetComponent(typeof(MeshRenderer)) as MeshRenderer;
        // Set font of TextMesh component (it works according to the inspector)
        myFont = (Font)Resources.Load("Fonts/Modern Vision");
        textMeshComponent.font = myFont;
        // Set the text string of the TextMesh component (it works according to the inspector)
        textMeshComponent.color = UnityEngine.Color.white;
        textMeshComponent.transform.localScale = new Vector3(0.007f, 0.007f, 0.007f);
        textMeshComponent.fontSize = 100;
        textMeshComponent.anchor = TextAnchor.MiddleCenter;
        textMeshComponent.alignment = TextAlignment.Center;
        myTextObject.GetComponent<Renderer>().material = myFont.material;

        textMeshComponent.text = nameLabel;
        //instantiate the prefab bounding box
        boundingBox = GameObject.Instantiate(Resources.Load("Models/BB")) as GameObject;
        boundingBox.GetComponentInChildren<MeshRenderer>().material.SetColor("_Color", UnityEngine.Color.white);
        boundingBox.GetComponentInChildren<MeshRenderer>().material.EnableKeyword("_EMISSION");
        boundingBox.GetComponentInChildren<MeshRenderer>().material.SetColor("_EmissionColor", UnityEngine.Color.white);
        width = face.faceRectangle.width;
        height = face.faceRectangle.height;
        resX = resolution.width;
        resY = resolution.height;
        boundingBox.transform.localScale = new Vector3((float)(width) / resY,
                                                       (float)(height) / resY,
                                                       (float)(width) / resY);
        if (displayBoundingBox)
        {
            boundingBox.SetActive(true);
        }
        else
        {
            boundingBox.SetActive(false);
        }

        boxCollider = myTextObject.AddComponent<BoxCollider>();
        boxCollider.isTrigger = true;

        UpdatePosPx(face);
        SetPosRotLabel(projectionMatrix, cameraToWorldMatrix);
    }

    public Label(CognitiveServices.Object obj, Resolution resolution, bool displayBoundingBox, Matrix4x4 projectionMatrix, Matrix4x4 cameraToWorldMatrix, int ID)
    {
        isStaticObject = false;
        countStaticObject = 0;
        isObj = true;
        isFace = false;
        nameLabel = obj.name + " " + ID;
        m_cameraResolution = resolution;
        // Create new object to display 3d text
        myTextObject = new GameObject(nameLabel);
        myTextObject.AddComponent<TextMesh>();
        
        // Get components
        textMeshComponent = myTextObject.GetComponent(typeof(TextMesh)) as TextMesh;
        meshRendererComponent = myTextObject.GetComponent(typeof(MeshRenderer)) as MeshRenderer;
        // Set font of TextMesh component (it works according to the inspector)
        myFont = (Font)Resources.Load("Fonts/Modern Vision");
        textMeshComponent.font = myFont;
        // Set the text string of the TextMesh component (it works according to the inspector)
        textMeshComponent.color = UnityEngine.Color.red;
        textMeshComponent.transform.localScale = new Vector3(0.007f, 0.007f, 0.007f);
        textMeshComponent.fontSize = 100;
        textMeshComponent.anchor = TextAnchor.MiddleCenter;
        textMeshComponent.alignment = TextAlignment.Center;
        myTextObject.GetComponent<Renderer>().material = myFont.material;

        textMeshComponent.text = obj.name;
        //instantiate the prefab bounding box
        boundingBox = GameObject.Instantiate(Resources.Load("Models/BB")) as GameObject;
        boundingBox.GetComponentInChildren<MeshRenderer>().material.SetColor("_Color", UnityEngine.Color.red);
        boundingBox.GetComponentInChildren<MeshRenderer>().material.EnableKeyword("_EMISSION");
        boundingBox.GetComponentInChildren<MeshRenderer>().material.SetColor("_EmissionColor", UnityEngine.Color.red);
        /*boundingBox.transform.localScale = new Vector3((float)(obj.box.width) * 1.39f / resolution.height,
                                                       (float)(obj.box.height) * 1.39f / resolution.height,
                                                       (float)(obj.box.width) * 1.39f / resolution.height);*/
        width = obj.box.width;
        height = obj.box.height;
        resX = resolution.width;
        resY = resolution.height;
        /*boundingBox.transform.localScale = new Vector3((float)(width) / resY,
                                                       (float)(height) / resY,
                                                       (float)(width) / resY);*/
        boundingBox.transform.localScale = new Vector3((float)(width) / resX,
                                                        (float)(height) / resX,
                                                        (float)(width) / resX);
        if (displayBoundingBox)
        {
            boundingBox.SetActive(true);
        }
        else
        {
            boundingBox.SetActive(false);
        }

        boxCollider = myTextObject.AddComponent<BoxCollider>();
        boxCollider.isTrigger = true;

        UpdatePosPx(obj);
        SetPosRotLabel(projectionMatrix, cameraToWorldMatrix);
    }

    public void Destroy()
    {
        Destroy(myTextObject);
        Destroy(boundingBox);
    }

    public void UpdatePosPx(Face face)
    {
        //set position face label
        Vector3 topLeft = CalcTopLeftVector(face);
        Vector3 topRight = CalcTopRightVector(face);
        Vector3 topMiddle = (topLeft + topRight) / 2.0f;
        Vector3 bottomLeft = CalcBottomLeftVector(face);
        Vector3 bottomRight = CalcBottomRightVector(face);
        Vector3 center = (topLeft + topRight + bottomLeft + bottomRight) / 4.0f;

        //calculate the distance in fonction of the face height in px
        distance = DistanceCamToFace(face.faceRectangle.height);

        pos = topMiddle;
        pos.y += 0.14f;  //add offset to get the tag above the head 

        //bounding box 
        posBB = center;
    }

    public void UpdatePosPx(CognitiveServices.Object obj)
    {
        //set position object label
        Vector3 topLeft = CalcTopLeftVector(obj);
        Vector3 topRight = CalcTopRightVector(obj);
        Vector3 bottomLeft = CalcBottomLeftVector(obj);
        Vector3 bottomRight = CalcBottomRightVector(obj);
        Vector3 center = (topLeft + topRight + bottomLeft + bottomRight) / 4.0f;

        //calculate the distance
        distance = 1.0f;

        pos = center;

        //bounding box 
        posBB = center;

        //box collider
        //Debug.Log("box collider pos " + boxCollider.transform.position);
    }

    public void SetPosRotLabel(Matrix4x4 projectionMatrix, Matrix4x4 cameraToWorldMatrix)
    {
        Vector3 ccCameraSpacePos = UnProjectVector(projectionMatrix, new Vector3(pos.x, pos.y, 1.0f)); //normalize vector
        Vector3 ccCameraSpacePosBB = UnProjectVector(projectionMatrix, new Vector3(posBB.x, posBB.y, 1.0f));
        //position = AveragePosition(position);
        bool isMoving = LabelIsMoving(textMeshComponent.transform.position);
        //if (LabelIsMoving(ccCameraSpacePos) || currentLerpTimeMove != lerpTimeMove)
        if ((isMoving || currentLerpTimeMove != lerpTimeMove) && !isStaticObject)
        {            
            if (isObj) //object part
            {
                //position
                Vector3 position = cameraToWorldMatrix.MultiplyPoint3x4(ccCameraSpacePos);
                Vector3 positionBB = cameraToWorldMatrix.MultiplyPoint3x4(ccCameraSpacePosBB);
                Vector3 direction = position - Camera.main.transform.position;
                RaycastHit hitInfo;

                if (Physics.Raycast(position, direction, out hitInfo,
                    30.0f, SpatialMapping.PhysicsRaycastMask))
                {
                    // Move this object's parent object to
                    // where the raycast hit the Spatial Mapping mesh.
                    position = hitInfo.point;
                    distance = (position - Camera.main.transform.position).magnitude;
                    //textMeshComponent.text = nameLabel + " " + distance.ToString();
                    //textMeshComponent.text = nameLabel + " " + isMoving;
                    textMeshComponent.transform.position = LerpMove(position);
                    textMeshComponent.color = UnityEngine.Color.red;
                    boundingBox.transform.position = textMeshComponent.transform.position;
                }
                else
                {
                    // put the label at 2 meters
                    distance = 2.0f;
                    ccCameraSpacePos *= distance;
                    ccCameraSpacePosBB *= distance;
                    //ccCameraSpacePos = AveragePosition(ccCameraSpacePos);
                    //position
                    position = cameraToWorldMatrix.MultiplyPoint3x4(ccCameraSpacePos);
                    positionBB = cameraToWorldMatrix.MultiplyPoint3x4(ccCameraSpacePosBB);
                    textMeshComponent.transform.position = LerpMove(position);
                    //textMeshComponent.text = nameLabel + " " + distance.ToString();
                    //textMeshComponent.text = nameLabel + " " + isMoving;
                    textMeshComponent.color = UnityEngine.Color.blue;
                    boundingBox.transform.position = textMeshComponent.transform.position;
                    boundingBox.GetComponentInChildren<MeshRenderer>().material.SetColor("_Color", UnityEngine.Color.blue);
                    boundingBox.GetComponentInChildren<MeshRenderer>().material.EnableKeyword("_EMISSION");
                    boundingBox.GetComponentInChildren<MeshRenderer>().material.SetColor("_EmissionColor", UnityEngine.Color.blue);
                }
            }
            else //face part
            {
                ccCameraSpacePos *= distance;
                ccCameraSpacePosBB *= distance;
                //ccCameraSpacePos = AveragePosition(ccCameraSpacePos);
                //position
                Vector3 position = cameraToWorldMatrix.MultiplyPoint3x4(ccCameraSpacePos);
                Vector3 positionBB = cameraToWorldMatrix.MultiplyPoint3x4(ccCameraSpacePosBB);
                //textMeshComponent.transform.position = position;
                //boundingBox.transform.position = positionBB;
                //textMeshComponent.text = nameLabel + " " + distance.ToString();
                //textMeshComponent.text = nameLabel + " " + isMoving;
                textMeshComponent.transform.position = LerpMove(position);
                boundingBox.transform.position = LerpMoveBoundingBox(positionBB);
            }
            //scale bounding box
            /*boundingBox.transform.localScale = new Vector3((float)(width) * distance / resY,
                                                           (float)(height) * distance / resY,
                                                           (float)(width) * distance / resY);*/
            boundingBox.transform.localScale = new Vector3((float)(width) * distance / resX,
                                                            (float)(height) * distance / resX,
                                                            (float)(width) * distance / resX);
            // Rotate the canvas object so that it faces the user.
            //Quaternion rotation = Quaternion.LookRotation(-cameraToWorldMatrix.GetColumn(2), cameraToWorldMatrix.GetColumn(1)); //follow head rotation
            //Quaternion rotation = Quaternion.LookRotation(-cameraToWorldMatrix.GetColumn(2)); // strictly horizontal
            //Vector3 direction = Camera.main.transform.forward;        
            //Quaternion rotation = Quaternion.LookRotation(direction); // strictly horizontal
            Quaternion rotation = Quaternion.LookRotation(Camera.main.transform.forward); // strictly horizontal            
            boundingBox.transform.rotation = Quaternion.Euler(0, rotation.eulerAngles.y, 0);
        }
        textMeshComponent.transform.rotation = Quaternion.LookRotation(Camera.main.transform.forward); // strictly horizontal

        IsOver();
    }

    Vector3 LerpMove(Vector3 destPos)
    {
        if (startPos == Vector3.zero)
        {
            startPos = destPos;
            endPos = destPos;
            return destPos;
        }
        //increment timer once per frame
        currentLerpTimeMove += Time.deltaTime;

        if (currentLerpTimeMove > lerpTimeMove)
        {
            currentLerpTimeMove = lerpTimeMove;
        }
        if (destPos != endPos && currentLerpTimeMove == lerpTimeMove)
        {
            startPos = endPos;
            endPos = destPos;
            currentLerpTimeMove = 0f;
        }
        //lerp!
        float perc = currentLerpTimeMove / lerpTimeMove;
        return Vector3.Lerp(startPos, endPos, perc);
    }

    Vector3 LerpMoveBoundingBox(Vector3 destPos)
    {
        if (startPosBB == Vector3.zero)
        {
            startPosBB = destPos;
            endPosBB = destPos;
            return destPos;
        }
        //increment timer once per frame
        currentLerpTimeMoveBB += Time.deltaTime;

        if (currentLerpTimeMoveBB > lerpTimeMoveBB)
        {
            currentLerpTimeMoveBB = lerpTimeMoveBB;
        }
        if (destPos != endPosBB && currentLerpTimeMoveBB == lerpTimeMoveBB)
        {
            startPosBB = endPosBB;
            endPosBB = destPos;
            currentLerpTimeMoveBB = 0f;
        }
        //lerp!
        float perc = currentLerpTimeMoveBB / lerpTimeMoveBB;
        return Vector3.Lerp(startPosBB, endPosBB, perc);
    }

    // here we detect if the face detected is moving more than a percentage
    bool LabelIsMoving(Vector3 posLabel)
    {
        bool isMoving;
        float threshold = 0.1f;
        float ratioX = Math.Abs(posLabel.x - histPos.x);
        float ratioY = Math.Abs(posLabel.y - histPos.y);
        float ratioZ = Math.Abs(posLabel.z - histPos.z);

        if (ratioX > threshold || ratioY > threshold || ratioZ > threshold)
        {
            isMoving = true;
            histPos = posLabel;
        }
        else isMoving = false;

        return isMoving;
    }

    /// <summary>
    /// Create the top left vector
    /// </summary>
    Vector3 CalcTopLeftVector(Face node)
    {
        Vector3 vector;
        vector = new Vector3(node.faceRectangle.left, node.faceRectangle.top, 0);
        return ScaleVector(vector);
    }

    /// <summary>
    /// Create the top right vector
    /// </summary>
    Vector3 CalcTopRightVector(Face node)
    {
        Vector3 vector;

        vector = new Vector3(node.faceRectangle.left + node.faceRectangle.width, node.faceRectangle.top, 0);
        return ScaleVector(vector);
    }

    /// <summary>
    /// Create the bottom right vector
    /// </summary>
    Vector3 CalcBottomRightVector(Face node)
    {
        Vector3 vector;
        vector = new Vector3(node.faceRectangle.left + node.faceRectangle.width, node.faceRectangle.top + node.faceRectangle.height, 0);
        return ScaleVector(vector);
    }

    /// <summary>
    /// Create the bottom left vector
    /// </summary>
    Vector3 CalcBottomLeftVector(Face node)
    {
        Vector3 vector;
        vector = new Vector3(node.faceRectangle.left, node.faceRectangle.top + node.faceRectangle.height, 0);
        return ScaleVector(vector);
    }

    /// <summary>
    /// Create the top left vector
    /// </summary>
    Vector3 CalcTopLeftVector(CognitiveServices.Object node)
    {
        Vector3 vector;
        vector = new Vector3(node.box.left, node.box.top, 0);
        return ScaleVector(vector);
    }

    /// <summary>
    /// Create the top right vector
    /// </summary>
    Vector3 CalcTopRightVector(CognitiveServices.Object node)
    {
        Vector3 vector;

        vector = new Vector3(node.box.left + node.box.width, node.box.top, 0);
        return ScaleVector(vector);
    }

    /// <summary>
    /// Create the bottom right vector
    /// </summary>
    Vector3 CalcBottomRightVector(CognitiveServices.Object node)
    {
        Vector3 vector;
        vector = new Vector3(node.box.left + node.box.width, node.box.top + node.box.height, 0);
        return ScaleVector(vector);
    }

    /// <summary>
    /// Create the bottom left vector
    /// </summary>
    Vector3 CalcBottomLeftVector(CognitiveServices.Object node)
    {
        Vector3 vector;
        vector = new Vector3(node.box.left, node.box.top + node.box.height, 0);
        return ScaleVector(vector);
    }

    /// <summary>
    /// Scale the vector
    /// </summary>
    Vector3 ScaleVector(Vector3 vector)
    {
        float scaleX = 1.0f * (2.0f * ((float)vector.x / (float)m_cameraResolution.width) - 1.0f);
        float scaleY = -1.0f * (2.0f * ((float)vector.y / (float)m_cameraResolution.height) - 1.0f);

        return new Vector3(scaleX, scaleY, 1.0f);
    }

    float DistanceCamToFace(float heightFace)
    {
        float distance;
        // distance (mm) = f(mm) * realHeight (mm) * imageHeight (px) /( objectHeight (px) * sensorHeight (mm))
        // average face nearly 220mm or 210mm
        // height found at 1000mm 170 px
        // f/SH = d * OH / (RH * IH) = 1000 *170 / (220*504) = 0.15333 
        // f/SH = d * OH / (RH * IH) = 1000 *170 / (210*504) = 0.16061 
        //distance = 0.153f * 220.0f * 504.0f / heightFace;
        distance = 0.16061f * 210.0f * 504.0f / heightFace;
        distance /= 100.0f; //convert mm into m
        return distance;
    }

    private Vector3 UnProjectVector(Matrix4x4 proj, Vector3 to)
    {
        Vector3 from = new Vector3(0, 0, 0);
        var axsX = proj.GetRow(0);
        var axsY = proj.GetRow(1);
        var axsZ = proj.GetRow(2);
        from.z = to.z / axsZ.z;
        from.y = (to.y - (from.z * axsY.z)) / axsY.y;
        from.x = (to.x - (from.z * axsX.z)) / axsX.x;
        return from;
    }
    
    private Vector3 AveragePosition(Vector3 pos)
    {
        countAverage++;

        if (countAverage > maxAverageSize)
        {
            bufferAverage.Add(pos);
            bufferAverage.RemoveAt(0);
        }
        else
        {
            bufferAverage.Add(pos);
        }

        Vector3 tmpAvg = new Vector3(0.0f, 0.0f, 0.0f);

        foreach (var buf in bufferAverage)
        {
            tmpAvg += buf / bufferAverage.Count;
        }

        return tmpAvg;
    }

    private float AverageDistance(float distance)
    {
        countAverageDistance++;

        if (countAverageDistance > maxAverageSize)
        {
            bufferAverageDistance.Add(distance);
            bufferAverageDistance.RemoveAt(0);
        }
        else
        {
            bufferAverageDistance.Add(distance);
        }

        float tmpAvg = 0.0f;

        foreach (var buf in bufferAverageDistance)
        {
            tmpAvg += buf / bufferAverageDistance.Count;
        }

        return tmpAvg;
    }

    public bool isSamePos(CognitiveServices.Object obj, Matrix4x4 projectionMatrix, Matrix4x4 cameraToWorldMatrix)
    {
        bool isSameObj = false;
        Vector3 testPos;
        //set position object label
        Vector3 topLeft = CalcTopLeftVector(obj);
        Vector3 topRight = CalcTopRightVector(obj);
        Vector3 bottomLeft = CalcBottomLeftVector(obj);
        Vector3 bottomRight = CalcBottomRightVector(obj);
        Vector3 center = (topLeft + topRight + bottomLeft + bottomRight) / 4.0f;

        testPos = center;

        Vector3 ccCameraSpacePos = UnProjectVector(projectionMatrix, new Vector3(testPos.x, testPos.y, 1.0f)); //normalize vector

        //position
        Vector3 position = cameraToWorldMatrix.MultiplyPoint3x4(ccCameraSpacePos);
        Vector3 direction = position - Camera.main.transform.position;
        RaycastHit hitInfo;

        if (Physics.Raycast(position, direction, out hitInfo,
            30.0f, SpatialMapping.PhysicsRaycastMask))
        {
            // Move this object's parent object to
            // where the raycast hit the Spatial Mapping mesh.
            position = hitInfo.point;
            distance = (position - Camera.main.transform.position).magnitude;
        }
        else return false;

        if (comparePositionLabel(position,  0.3f))
        {
            isSameObj = true;
            countStaticObject++;
            if (countStaticObject >= 6)
            {
                isStaticObject = true;
                textMeshComponent.color = UnityEngine.Color.green;
                boundingBox.GetComponentInChildren<MeshRenderer>().material.SetColor("_Color", UnityEngine.Color.green);
                boundingBox.GetComponentInChildren<MeshRenderer>().material.EnableKeyword("_EMISSION");
                boundingBox.GetComponentInChildren<MeshRenderer>().material.SetColor("_EmissionColor", UnityEngine.Color.green);
                //textMeshComponent.text = nameLabel + textMeshComponent.transform.position;
            }            
        }

        return isSameObj;
    }
    // we compare the position sent to the label position in function of delta range
    private bool comparePositionLabel(Vector3 position, float delta)
    {
        bool isInside = false;
        Vector3 posLabel = textMeshComponent.transform.position;
        //Vector3 posLabel = pos;
        //Vector3 posLabel = endPos;
        //Debug.Log("pos " + posLabel + " test " + position + " dif " + (posLabel - position).ToString());
        //textMeshComponent.text = nameLabel + " " + (posLabel - position).ToString();
        if (position.x >= (posLabel.x - delta) && position.x <= (posLabel.x + delta) &&
            position.y >= (posLabel.y - delta) && position.y <= (posLabel.y + delta) &&
            position.z >= (posLabel.z - delta) && position.z <= (posLabel.z + delta))
        {
            isInside = true;
            //Debug.Log("object inside"); 
        }

        return isInside;
    }

    public void IsOver()
    {
        // Do a raycast into the world based on the user's
        // head position and orientation.
        var headPosition = Camera.main.transform.position;
        var gazeDirection = Camera.main.transform.forward;

        RaycastHit hitInfo;
        if (Physics.Raycast(headPosition, gazeDirection, out hitInfo))
        {
            if (hitInfo.collider.gameObject.name.Equals(nameLabel))
            {
                textMeshComponent.color = UnityEngine.Color.white;
                isOver = true;
            }
            else
            {
                textMeshComponent.color = UnityEngine.Color.red;
                isOver = false;
            }
        }
    }

    public void Tapped()
    {
        Debug.Log("tapped clic");
    }
}
