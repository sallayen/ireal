﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.XR.WSA.Input;

public class HandsDetected : MonoBehaviour {

    void Awake()
    {
        InteractionManager.InteractionSourceDetected += handsDetection;
        InteractionManager.InteractionSourceLost += handsLost;
    }

    public void Start()
    {
        GameObject.Find("Menu").SetActive(false);
    }

    void handsDetection(InteractionSourceDetectedEventArgs args)
    {
        uint id = args.state.source.id;
        // Check to see that the source is a hand.
        if (args.state.source.kind != InteractionSourceKind.Hand)
        {
            return;
        }
        GameObject.Find("Menu").SetActive(true);
        Debug.Log("hands detected: " + id);
    }

    void handsLost(InteractionSourceLostEventArgs args)
    {
        uint id = args.state.source.id;
        // Check to see that the source is a hand.
        if (args.state.source.kind != InteractionSourceKind.Hand)
        {
            return;
        }
        GameObject.Find("Menu").SetActive(false);
        Debug.Log("hands lost: " + id);
    }

    void OnDestroy()
    {
        InteractionManager.InteractionSourceDetected += handsDetection;
        InteractionManager.InteractionSourceLost += handsLost;
    }
}
