﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSelected : MonoBehaviour {

    public GameObject hud;

    public void ObjectTapped()
    {
        Debug.Log("Object");
        string address = "http://titanic.paris.inria.fr:9099/api/cv/object_recognition/";
        hud.GetComponent<Hud>().ResetFaceParameters();
        hud.GetComponent<Hud>()._computerVisionEndpoint = address;        
        Debug.Log("adress is " + address);
    }
}
