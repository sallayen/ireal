﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpatialMappingSelected : MonoBehaviour {

    bool drawSM;
    public GameObject spatialMapping;
    public void Start()
    {
        drawSM = false;
    }

    public void SpatialMappingTapped()
    {
        Debug.Log("SpatialMapping");
        drawSM = !drawSM;
        spatialMapping.GetComponent<SpatialMapping>().DrawVisualMeshes = drawSM;
    }
}
