﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundingBoxSelected : MonoBehaviour {

    public GameObject hud;

    public void BoundingBoxTapped()
    {
        if (hud.GetComponent<Hud>().displayBB)
        {
            hud.GetComponent<Hud>().BoundingBoxIsOff();
            Debug.Log("BoundingBox is off");
        }
        else
        {
            hud.GetComponent<Hud>().BoundingBoxIsOn();
            Debug.Log("BoundingBox is on");
        }        
    }
}
