﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceObjectSelected : MonoBehaviour {

    public GameObject hud;

	public void FaceObjectTapped()
    {
        Debug.Log("FaceObject");
        string address = "http://titanic.paris.inria.fr:9099/api/cv/face_object_recognition/";        
        hud.GetComponent<Hud>()._computerVisionEndpoint = address;
        Debug.Log("adress is " + address);
    }
}
