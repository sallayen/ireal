﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowLinesSelected : MonoBehaviour {

    public GameObject hud;

    public void Start()
    {

    }

    public void ShowLinesTapped()
    {
        hud.GetComponent<Hud>().showLines = !hud.GetComponent<Hud>().showLines;
        if (hud.GetComponent<Hud>().showLines)
        {
            Debug.Log("ShowLines is On");
        }
        else
        {
            hud.GetComponent<Hud>().ClearLines();
            Debug.Log("ShowLines is Off");
        }
    }
}
