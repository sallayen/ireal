﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaptureSelected : MonoBehaviour {

    public GameObject hud;
    public void Start()
    {

    }

    public void CaptureTapped()
    {
        hud.GetComponent<Hud>().captureOnOff = !hud.GetComponent<Hud>().captureOnOff;
        Debug.Log("capture " + hud.GetComponent<Hud>().captureOnOff);
        if (hud.GetComponent<Hud>().captureOnOff)
        {
            hud.GetComponent<Hud>().StartPhotoMode();
            Debug.Log("Capture is On");
        }
        else
        {
            hud.GetComponent<Hud>().StopPhotoMode();
            Debug.Log("Capture is Off");
        }
    }
}
