﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugSelected : MonoBehaviour {

    bool drawDebug;
    public GameObject debug;

    public void Start()
    {
        drawDebug = true;
        debug.SetActive(drawDebug);
    }

    public void DebugTapped()
    {
        Debug.Log("Debug");
        drawDebug = !drawDebug;
        debug.SetActive(drawDebug);
        Debug.Log("debug: " + drawDebug);
    }
}
