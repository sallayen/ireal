﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticObjectSelected : MonoBehaviour {

    public GameObject staticObject;
    public void Start()
    {

    }

    public void StaticObjectTapped()
    {        
        staticObject.GetComponent<Hud>().staticObjectMode = !staticObject.GetComponent<Hud>().staticObjectMode;

        if (staticObject.GetComponent<Hud>().staticObjectMode)
        {
            staticObject.GetComponent<Hud>().ResetObjParameters();
            Debug.Log("Static Object Mode is On");
        }            
        else
        {
            Debug.Log("Static Object Mode is Off");
        }            
    }
}
