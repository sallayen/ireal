﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceSelected : MonoBehaviour {

    public GameObject hud;

    public void FaceTapped()
    {
        Debug.Log("Face");
        string address = "http://titanic.paris.inria.fr:9099/api/cv/face_recognition/";
        hud.GetComponent<Hud>().ResetObjParameters();
        hud.GetComponent<Hud>()._computerVisionEndpoint = address;        
        Debug.Log("adress is " + address);
    }
}
