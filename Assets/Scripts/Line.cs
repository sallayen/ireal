﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Line : MonoBehaviour
{
    public Material lineMat;
    public float radius = 0.005f;
    public Vector3 start;
    public Vector3 end;
    private GameObject cylinder;
    public Transform cylinderPrefab;
    Material mat;
    Resolution m_cameraResolution;
    /*
    Line()
    {
        start = new Vector3(-0.7f, 0.4f, 3f);
        end = new Vector3(0.5f, 0.5f, 3f);
        //cylinder = Instantiate<GameObject>(cylinderPrefab.gameObject, Vector3.zero, Quaternion.identity);
        cylinder = Instantiate(Resources.Load("Cylinder", typeof(GameObject))) as GameObject;
        mat = Resources.Load("Cylinders", typeof(Material)) as Material;
        cylinder.GetComponent<Renderer>().material = mat;
    }*/

    public Line(Vector3 posStart, Vector3 posEnd)
    {
        start = posStart;
        end = posEnd;        
        UpdateCylinderPosition(start, end);
        Debug.Log("Line created");
        //cylinder = Instantiate<GameObject>(cylinderPrefab.gameObject, Vector3.zero, Quaternion.identity);
        /*cylinder = Instantiate(Resources.Load("Cylinder", typeof(GameObject))) as GameObject;
        mat = Resources.Load("Cylinders", typeof(Material)) as Material;
        cylinder.GetComponent<Renderer>().material = mat;*/
    }

    public Line(CognitiveServices.Object obj, Resolution res, Matrix4x4 projectionMatrix, Matrix4x4 cameraToWorldMatrix)
    {        
        m_cameraResolution = res;
        UpdateCylinderPosition(start, end);
        CalculatePosLine(obj,  projectionMatrix,  cameraToWorldMatrix);
        Debug.Log("Line created");
        //cylinder = Instantiate<GameObject>(cylinderPrefab.gameObject, Vector3.zero, Quaternion.identity);
        /*cylinder = Instantiate(Resources.Load("Cylinder", typeof(GameObject))) as GameObject;
        mat = Resources.Load("Cylinders", typeof(Material)) as Material;
        cylinder.GetComponent<Renderer>().material = mat;*/
    }

    void Start()
    {        
        /*start = new Vector3(-0.7f, 0.4f, 3f);
        end = new Vector3(0.5f, 0.5f, 3f);*/
        //cylinder = Instantiate<GameObject>(cylinderPrefab.gameObject, Vector3.zero, Quaternion.identity);
        /*cylinder = Instantiate(Resources.Load("Models/Cylinder", typeof(GameObject))) as GameObject;
        mat = Resources.Load("Materials/Cylinders", typeof(Material)) as Material;
        cylinder.GetComponent<Renderer>().material = mat;*/
    }

    void Update()
    {
        //UpdateCylinderPosition(start, end);
        //Debug.Log("start end " + start + " " + end);
    }

    private void UpdateCylinderPosition(Vector3 beginPoint, Vector3 endPoint)
    {
        cylinder = Instantiate(Resources.Load("Models/Cylinder", typeof(GameObject))) as GameObject;
        mat = Resources.Load("Materials/Cylinders", typeof(Material)) as Material;
        cylinder.GetComponent<Renderer>().material = mat;
        Vector3 offset = endPoint - beginPoint;
        Vector3 position = beginPoint + (offset / 2.0f);
        cylinder.transform.position = position;
        cylinder.transform.LookAt(beginPoint, Vector3.up);
        cylinder.transform.localScale = new Vector3(radius, radius, (endPoint - beginPoint).magnitude);
        cylinder.transform.rotation *= Quaternion.Euler(0, 0, 90);
    }

    public void CalculatePosLine(CognitiveServices.Object obj, Matrix4x4 projectionMatrix, Matrix4x4 cameraToWorldMatrix)
    {
        //set position object label
        Vector3 topLeft = CalcTopLeftVector(obj);
        Vector3 topRight = CalcTopRightVector(obj);
        Vector3 bottomLeft = CalcBottomLeftVector(obj);
        Vector3 bottomRight = CalcBottomRightVector(obj);
        Vector3 center = (topLeft + topRight + bottomLeft + bottomRight) / 4.0f;

        end = center;
        
        Vector3 ccCameraSpacePos = UnProjectVector(projectionMatrix, new Vector3(end.x, end.y, 1.0f)); //normalize vector

        //position
        Vector3 position = cameraToWorldMatrix.MultiplyPoint3x4(ccCameraSpacePos);
        Vector3 direction = position - Camera.main.transform.position;
        RaycastHit hitInfo;

        if (Physics.Raycast(position, direction, out hitInfo,
            30.0f, SpatialMapping.PhysicsRaycastMask))
        {
            // Move this object's parent object to
            // where the raycast hit the Spatial Mapping mesh.
            position = hitInfo.point;
            start = Camera.main.transform.position;
            end = position;

            UpdateCylinderPosition(start, end);
        }
    }
    
    /// <summary>
    /// Create the top left vector
    /// </summary>
    Vector3 CalcTopLeftVector(CognitiveServices.Object node)
    {
        Vector3 vector;
        vector = new Vector3(node.box.left, node.box.top, 0);
        return ScaleVector(vector);
    }

    /// <summary>
    /// Create the top right vector
    /// </summary>
    Vector3 CalcTopRightVector(CognitiveServices.Object node)
    {
        Vector3 vector;

        vector = new Vector3(node.box.left + node.box.width, node.box.top, 0);
        return ScaleVector(vector);
    }

    /// <summary>
    /// Create the bottom right vector
    /// </summary>
    Vector3 CalcBottomRightVector(CognitiveServices.Object node)
    {
        Vector3 vector;
        vector = new Vector3(node.box.left + node.box.width, node.box.top + node.box.height, 0);
        return ScaleVector(vector);
    }

    /// <summary>
    /// Create the bottom left vector
    /// </summary>
    Vector3 CalcBottomLeftVector(CognitiveServices.Object node)
    {
        Vector3 vector;
        vector = new Vector3(node.box.left, node.box.top + node.box.height, 0);
        return ScaleVector(vector);
    }

    /// <summary>
    /// Scale the vector
    /// </summary>
    Vector3 ScaleVector(Vector3 vector)
    {
        float scaleX = 1.0f * (2.0f * ((float)vector.x / (float)m_cameraResolution.width) - 1.0f);
        float scaleY = -1.0f * (2.0f * ((float)vector.y / (float)m_cameraResolution.height) - 1.0f);

        return new Vector3(scaleX, scaleY, 1.0f);
    }

    private Vector3 UnProjectVector(Matrix4x4 proj, Vector3 to)
    {
        Vector3 from = new Vector3(0, 0, 0);
        var axsX = proj.GetRow(0);
        var axsY = proj.GetRow(1);
        var axsZ = proj.GetRow(2);
        from.z = to.z / axsZ.z;
        from.y = (to.y - (from.z * axsY.z)) / axsY.y;
        from.x = (to.x - (from.z * axsX.z)) / axsX.x;
        return from;
    }

    public void Destroy()
    {
        Destroy(cylinder);
    }
}
