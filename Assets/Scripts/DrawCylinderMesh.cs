﻿using UnityEngine;
using System.Collections;

public class DrawCylinderMesh : MonoBehaviour
{

    // Material used for the connecting lines
    public Material lineMat;

    public float radius = 0.05f;

    // Connect all of the `points` to the `mainPoint`
    public GameObject mainPoint;
    public GameObject[] points;

    // Fill in this with the default Unity Cylinder mesh
    // We will account for the cylinder pivot/origin being in the middle.
    public Mesh cylinderMesh;
    GameObject line;
    public Vector3 start;
    public Vector3 end;
    GameObject cylinder;
    /*
    DrawCylinderMesh ()
    {
        Vector3 start = new Vector3(-0.5f, -0.5f, 2f);
        Vector3 end = new Vector3(0.5f, 0.5f, 2f);
        SetCylinderMesh(start, end);
    }

    DrawCylinderMesh(Vector3 start, Vector3 end)
    {
        posStart = start;
        posEnd = end; 
        lineMat = Resources.Load("Materials/Cylinders.mat", typeof(Material)) as Material;
        
        // We make a offset gameobject to counteract the default cylindermesh pivot/origin being in the middle
        GameObject ringOffsetCylinderMeshObject = new GameObject();

        // Offset the cylinder so that the pivot/origin is at the bottom in relation to the outer ring gameobject.
        ringOffsetCylinderMeshObject.transform.localPosition = new Vector3(0f, 1f, 0f);
        // Set the radius
        ringOffsetCylinderMeshObject.transform.localScale = new Vector3(radius, 1f, radius);

        // Create the the Mesh and renderer to show the connecting ring
        MeshFilter ringMesh = ringOffsetCylinderMeshObject.AddComponent<MeshFilter>();
        ringMesh.mesh = this.cylinderMesh;
        //ringMesh.mesh = GameObject.CreatePrimitive(PrimitiveType.Cylinder).
        GameObject cylinder = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        ringMesh.mesh = cylinder.GetComponent<MeshFilter>().sharedMesh;

        MeshRenderer ringRenderer = ringOffsetCylinderMeshObject.AddComponent<MeshRenderer>();
        ringRenderer.material = lineMat;
    }

    public void SetCylinderMesh(Vector3 start, Vector3 end)
    {
        posStart = start;
        posEnd = end;
        lineMat = Resources.Load("Materials/Cylinders.mat", typeof(Material)) as Material;

        // We make a offset gameobject to counteract the default cylindermesh pivot/origin being in the middle
        GameObject ringOffsetCylinderMeshObject = new GameObject();

        // Offset the cylinder so that the pivot/origin is at the bottom in relation to the outer ring gameobject.
        ringOffsetCylinderMeshObject.transform.localPosition = new Vector3(0f, 1f, 0f);
        // Set the radius
        ringOffsetCylinderMeshObject.transform.localScale = new Vector3(radius, 1f, radius);

        // Create the the Mesh and renderer to show the connecting ring
        MeshFilter ringMesh = ringOffsetCylinderMeshObject.AddComponent<MeshFilter>();
        ringMesh.mesh = this.cylinderMesh;
        //ringMesh.mesh = GameObject.CreatePrimitive(PrimitiveType.Cylinder).
        GameObject cylinder = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        ringMesh.mesh = cylinder.GetComponent<MeshFilter>().sharedMesh;

        MeshRenderer ringRenderer = ringOffsetCylinderMeshObject.AddComponent<MeshRenderer>();
        ringRenderer.material = lineMat;
    }*/

    // Use this for initialization
    /*void Start()
    {
        // Make a gameobject that we will put the ring on
        // And then put it as a child on the gameobject that has this Command and Control script
        this.line = new GameObject();
        this.line.transform.parent = this.gameObject.transform;

        // We make a offset gameobject to counteract the default cylindermesh pivot/origin being in the middle
        GameObject ringOffsetCylinderMeshObject = new GameObject();
        ringOffsetCylinderMeshObject.transform.parent = this.line.transform;

        // Offset the cylinder so that the pivot/origin is at the bottom in relation to the outer ring gameobject.
        ringOffsetCylinderMeshObject.transform.localPosition = new Vector3(0f, 1f, 0f);
        // Set the radius
        ringOffsetCylinderMeshObject.transform.localScale = new Vector3(radius, 1f, radius);

        // Create the the Mesh and renderer to show the connecting ring
        MeshFilter ringMesh = ringOffsetCylinderMeshObject.AddComponent<MeshFilter>();
        //ringMesh.mesh = this.cylinderMesh;
        //ringMesh.mesh = GameObject.CreatePrimitive(PrimitiveType.Cylinder).
        GameObject cylinder = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        ringMesh.mesh = cylinder.GetComponent<MeshFilter>().sharedMesh;
        Destroy(cylinder);

        MeshRenderer ringRenderer = ringOffsetCylinderMeshObject.AddComponent<MeshRenderer>();
        ringRenderer.material = lineMat;

        start = new Vector3(-0.8f, -0.5f, 3f);
        end = new Vector3(0.5f, 0.5f, 3f);
        // Move the ring to the point
        //this.line.transform.position = this.points[i].transform.position;
        this.line.transform.position = start;

        // Match the scale to the distance
        //float cylinderDistance = 0.5f * Vector3.Distance(this.points[i].transform.position, this.mainPoint.transform.position);
        float cylinderDistance = 0.5f * Vector3.Distance(end , start);
        this.line.transform.localScale = new Vector3(this.line.transform.localScale.x, cylinderDistance, this.line.transform.localScale.z);

        // Make the cylinder look at the main point.
        // Since the cylinder is pointing up(y) and the forward is z, we need to offset by 90 degrees.
        this.line.transform.LookAt(this.mainPoint.transform, Vector3.up);
        this.line.transform.rotation *= Quaternion.Euler(90, 0, 0);
    }*/

    void Start()
    {
        start = new Vector3(-0.7f, 0.4f, 3f);
        end = new Vector3(0.5f, 0.5f, 3f);
        cylinder = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
        cylinder.transform.rotation *= Quaternion.Euler(90, 0, 0);
    }

    void Update()
    {
        UpdateCylinderPosition(start, end);
        Debug.Log("start end " + start + " " + end);
    }

    //private void UpdateCylinderPosition(GameObject cylinder, Vector3 beginPoint, Vector3 endPoint)
    private void UpdateCylinderPosition(Vector3 beginPoint, Vector3 endPoint)
    {
        Vector3 offset = endPoint - beginPoint;
        Vector3 position = beginPoint + (offset / 2.0f);

        cylinder.transform.position = position;
        cylinder.transform.LookAt(beginPoint);
        /*Vector3 localScale = cylinder.transform.localScale;
        localScale.z = (endPoint - beginPoint).magnitude;
        cylinder.transform.localScale = localScale;*/
        cylinder.transform.localScale = new Vector3(radius, radius, (endPoint - beginPoint).magnitude);
    }
    /*
    // Update is called once per frame
    void Update()
    {
        Vector3 start = new Vector3(-0.5f, -0.5f, 3f);
        Vector3 end = new Vector3(0.5f, 0.5f, 3f);
        // Move the ring to the point
        //this.line.transform.position = this.points[i].transform.position;
        this.line.transform.position = start;

        // Match the scale to the distance
        //float cylinderDistance = 0.5f * Vector3.Distance(this.points[i].transform.position, this.mainPoint.transform.position);
        float cylinderDistance = 0.5f * Vector3.Distance(start, end);
        this.line.transform.localScale = new Vector3(this.line.transform.localScale.x, cylinderDistance, this.line.transform.localScale.z);

        // Make the cylinder look at the main point.
        // Since the cylinder is pointing up(y) and the forward is z, we need to offset by 90 degrees.
        this.line.transform.LookAt(this.mainPoint.transform, Vector3.up);
        this.line.transform.rotation *= Quaternion.Euler(90, 0, 0);
    }*/
}

/*
using UnityEngine;
using System.Collections;

public class DrawCylinderMesh : MonoBehaviour
{

    // Material used for the connecting lines
    public Material lineMat;

    public float radius = 0.05f;

    // Connect all of the `points` to the `mainPoint`
    public GameObject mainPoint;
    public GameObject[] points;

    // Fill in this with the default Unity Cylinder mesh
    // We will account for the cylinder pivot/origin being in the middle.
    public Mesh cylinderMesh;


    GameObject[] ringGameObjects;
    
    // Use this for initialization
    void Start()
    {
        this.ringGameObjects = new GameObject[points.Length];
        //this.connectingRings = new ProceduralRing[points.Length];
        for (int i = 0; i < points.Length; i++)
        {
            // Make a gameobject that we will put the ring on
            // And then put it as a child on the gameobject that has this Command and Control script
            this.ringGameObjects[i] = new GameObject();
            this.ringGameObjects[i].name = "Connecting ring #" + i;
            this.ringGameObjects[i].transform.parent = this.gameObject.transform;

            // We make a offset gameobject to counteract the default cylindermesh pivot/origin being in the middle
            GameObject ringOffsetCylinderMeshObject = new GameObject();
            ringOffsetCylinderMeshObject.transform.parent = this.ringGameObjects[i].transform;

            // Offset the cylinder so that the pivot/origin is at the bottom in relation to the outer ring gameobject.
            ringOffsetCylinderMeshObject.transform.localPosition = new Vector3(0f, 1f, 0f);
            // Set the radius
            ringOffsetCylinderMeshObject.transform.localScale = new Vector3(radius, 1f, radius);

            // Create the the Mesh and renderer to show the connecting ring
            MeshFilter ringMesh = ringOffsetCylinderMeshObject.AddComponent<MeshFilter>();
            ringMesh.mesh = this.cylinderMesh;

            MeshRenderer ringRenderer = ringOffsetCylinderMeshObject.AddComponent<MeshRenderer>();
            ringRenderer.material = lineMat;

        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < points.Length; i++)
        {
            // Move the ring to the point
            this.ringGameObjects[i].transform.position = this.points[i].transform.position;

            // Match the scale to the distance
            float cylinderDistance = 0.5f * Vector3.Distance(this.points[i].transform.position, this.mainPoint.transform.position);
            this.ringGameObjects[i].transform.localScale = new Vector3(this.ringGameObjects[i].transform.localScale.x, cylinderDistance, this.ringGameObjects[i].transform.localScale.z);

            // Make the cylinder look at the main point.
            // Since the cylinder is pointing up(y) and the forward is z, we need to offset by 90 degrees.
            this.ringGameObjects[i].transform.LookAt(this.mainPoint.transform, Vector3.up);
            this.ringGameObjects[i].transform.rotation *= Quaternion.Euler(90, 0, 0);
        }
    }
}
*/