﻿using System;
namespace CognitiveServices
{
    [Serializable]
    public class Face
    {
        public int age;
        public FaceRectangle faceRectangle;
        public string gender;
        public string faceId;
    }
}