# iReal : Hololens projet


The goal of this projet is to build a demostration prototype of a smart personal
assistant using the Hololens device. The two main components here are facial and object recognition. 

## Installation

Minimum requirements
Windows 10 with SDK 10.017763.132 
Hololens 1 with OS 10.0.17763.380 
Unity 2017.4.16f1
The server part must be installed and configured as explained [here](https://gitlab.inria.fr/sallayen/py_ireal)

### Enable client server communication

* Etablish communication between the device and a processing server

### Global menu

If you reach out your arm you will see a global menu appearing which permits to choose between this different options: 

* **Capture** Enable or not the capture (default on)

* **Show Lines** Display or not the raycast lines from the camera to the object detected (default off)

* **Object** Only treat object recognition (default off)

* **Face & Object** Treat the object and facial recognition (default on)

* **Spatial Mapping** Display the 3D environment recronstructed by the hololens (default off)

* **Bounding Boxes** Display the bounding boxes of the object recognition. Here the depth of the 3D box is the same than width (default off)

* **Static Object** If more than 5 iterations of the same object are calculated the label object will be static (default on)

* **Debug** Display or not the debug log (default on)

